﻿using System;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace ST8.Editor
{
    public static partial class Utility
    {
        internal static Assembly[] Assemblies { get { if (null == assemblies) assemblies = AppDomain.CurrentDomain.GetAssemblies(); return assemblies; } }
        private static Assembly[] assemblies = null;

        public static GUIStyle ToolbarButton { get { return EditorStyles.toolbarButton; } }
        public static GUIStyle DepressedToolbarButton
        {
            get
            {
                if (null == depressedToolbarButton)
                {
                    depressedToolbarButton = new GUIStyle(EditorStyles.toolbarButton);
                    depressedToolbarButton.normal = depressedToolbarButton.active;
                }

                return depressedToolbarButton;
            }
        }
        static GUIStyle depressedToolbarButton = null;
    }
}
