﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEditor;

namespace ST8.Editor
{
    public static partial class Utility
    {
        public static partial class State
        {
            public static string[] Names { get { if (null == names) names = Types.Select(T => string.Format("{0} ({1})", T.Name, Regex.Replace(T.FullName, @"\+", @"."))).Append("None").ToArray(); return names; } }
            static string[] names = null;

            public static System.Type[] Types { get { if (null == types) types = EnumerateTypes(Assemblies.AsEnumerable().GetEnumerator()).ToArray(); return types; } }
            static System.Type[] types = null;

            static IEnumerable<System.Type> EnumerateTypes(IEnumerator<Assembly> assemblies)
            {
                if (null != assemblies)
                {
                    if (assemblies.MoveNext())
                    {
                        return assemblies.Current.GetTypes().Where(T => !T.IsAbstract && T.IsSubclassOf(typeof(ST8.State))).Concat(EnumerateTypes(assemblies));
                    }
                }

                return new System.Type[0];
            }
        }
    }
}
