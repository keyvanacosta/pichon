﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEditor;

namespace ST8.Editor
{
    public static partial class Utility
    {
        public static partial class State
        {
            public static class Type
            {
                public const string assemblyQualifiedName = nameof(State.Type.assemblyQualifiedName);

                public static SerializedProperty AssemblyQualifiedName(SerializedProperty property) { return property.FindPropertyRelative(assemblyQualifiedName); }

                public static bool Popup(Rect position, SerializedProperty property, GUIContent label)
                {
                    SerializedProperty assemblyQualifiedNameProperty = AssemblyQualifiedName(property);
                    string assemblyQualifiedName = assemblyQualifiedNameProperty.stringValue;
                    int index = Utility.State.Names.Length - 1; // last item in names list is None

                    if (!string.IsNullOrEmpty(assemblyQualifiedName))
                    {
                        System.Type type = Utility.State.Types.SingleOrDefault(T => T.AssemblyQualifiedName == assemblyQualifiedName);
                        if (null != type)
                        {
                            index = Array.IndexOf(Utility.State.Types, type);
                        }
                        else
                        {
                            assemblyQualifiedNameProperty.stringValue = string.Empty;

                            return true;
                        }
                    }

                    EditorGUI.BeginChangeCheck();
                    index = EditorGUI.Popup(position, label.text, index, Utility.State.Names);
                    if (EditorGUI.EndChangeCheck())
                    {
                        if (index < Utility.State.Types.Length)
                        {
                            assemblyQualifiedNameProperty.stringValue = Utility.State.Types[index].AssemblyQualifiedName;
                        }
                        else
                        {
                            assemblyQualifiedNameProperty.stringValue = string.Empty;
                        }

                        return true;
                    }

                    return false;
                }

                public static bool Popup(SerializedProperty property, GUIContent label, IReadOnlyList<ST8.State.Type> types)
                {
                    if (null != types)
                    {
                        SerializedProperty assemblyQualifiedNameProperty = AssemblyQualifiedName(property);
                        string assemblyQualifiedName = assemblyQualifiedNameProperty.stringValue;

                        string[] names = types.Select(T => string.IsNullOrEmpty(T.assemblyQualifiedName) ? "None" : string.Format("{0} ({1})", ((System.Type)T).Name, Regex.Replace(((System.Type)T).FullName, @"\+", @"."))).Append("None").ToArray();
                        int index = types.Count; // None == total # of types in list + 1

                        if (!string.IsNullOrEmpty(assemblyQualifiedName))
                        {
                            ST8.State.Type type = types.SingleOrDefault(T => T.assemblyQualifiedName == assemblyQualifiedName);
                            if (null != type)
                            {
                                for (int n = 0; n < types.Count; ++n)
                                    if (types[n] == type)
                                    {
                                        index = n;
                                        break;
                                    }
                            }
                            else
                            {
                                assemblyQualifiedNameProperty.stringValue = string.Empty;

                                return true;
                            }
                        }

                        EditorGUI.BeginChangeCheck();
                        index = EditorGUILayout.Popup(label.text, index, names);
                        if (EditorGUI.EndChangeCheck())
                        {
                            if (index < types.Count)
                            {
                                assemblyQualifiedNameProperty.stringValue = types[index].assemblyQualifiedName;
                            }
                            else
                            {
                                assemblyQualifiedNameProperty.stringValue = string.Empty;
                            }

                            return true;
                        }
                    }
                    else
                        EditorGUILayout.LabelField(label.text, "No available types.");

                    return false;
                }
            }
        }
    }
}
