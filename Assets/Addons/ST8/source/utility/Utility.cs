﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace ST8
{
    public static class Utility
    {
        public static void Add(this SortedList<int, State> states, State state) { states.Add(state.key, state); }

        public static T GetDelegate<T>(this object item, string name, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, params Type[] types) where T : class
        {
            MethodInfo methodInfo = item.GetMethod(name, flags, types);
            return null != methodInfo ? (T)Convert.ChangeType(Delegate.CreateDelegate(typeof(T), item, methodInfo), typeof(T)) : null;
        }

        static MethodInfo GetMethod(this object item, string methodName, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, params Type[] types)
        {
            return item.GetType().GetMethod(methodName, flags, null, types, null);
        }

        public static void AddUnique(this List<State.Type> @this, State.Type type)
        {
            for (int n = 0; n < @this.Count; ++n)
                if (@this[n].assemblyQualifiedName == type.assemblyQualifiedName)
                    return;

            @this.Add(type);
        }
    }
}
