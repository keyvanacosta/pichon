﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ST8
{
    [DisallowMultipleComponent]
    public abstract partial class Machine : MonoBehaviour
    {
        public virtual void Awake()
        {
            HandleLateUpdate();
            HandleOnCollision();
            HandleOnTrigger();
            HandleOnAnimator();
        }

        public virtual void Start()
        {
            if (null != (System.Type)initial)
                Change(initial.key);
        }

        public virtual void Update()
        {
            if (playing && null != current)
                current.UpdateDelegate();
        }

        public virtual void FixedUpdate()
        {
            if (playing && null != current)
                current.FixedUpdateDelegate();
        }

        void HandleLateUpdate()
        {
            foreach (State state in States.Values)
                if (state.hasLateUpdate)
                {
                    gameObject.AddComponent<LateUpdateMessageHandler>();
                    break;
                }
        }

        void HandleOnCollision()
        {
            foreach (State state in States.Values)
                if (state.hasOnCollision)
                {
                    gameObject.AddComponent<OnCollisionMessagesHandler>();
                    break;
                }
        }

        void HandleOnTrigger()
        {
            foreach (State state in States.Values)
                if (state.hasOnTrigger)
                {
                    gameObject.AddComponent<OnTriggerMessagesHandler>();
                    break;
                }
        }

        void HandleOnAnimator()
        {
            foreach (State state in States.Values)
                if (state.hasOnAnimator)
                {
                    gameObject.AddComponent<OnAnimatorMessagesHandler>();
                    break;
                }
        }

        public IReadOnlyDictionary<int, State> States
        {
            get
            {
                if (null == states)
                {
                    states = new Dictionary<int, State>(types.Count);
                    foreach (State.Type type in types)
                    {
                        if (null != type)
                        {
                            if (null != (System.Type)type)
                            {
                                State state = (State)Activator.CreateInstance((System.Type)type);
                                state.machine = this;
                                states.Add(state.key, state); 
                            }
                        }
                    }
                }

                return states;
            }
        }
        Dictionary<int, State> states = null;

        private State current { get; set; }

        [HideInInspector][SerializeField] public State.Type initial = new State.Type();
        public void Initial<T>() where T : ST8.State { initial = types.SingleOrDefault(s => ((System.Type)s) == typeof(T)); }

        public IReadOnlyList<State.Type> Types { get { return types; } }
        [SerializeField] List<State.Type> types = new List<State.Type>();

        public void RemoveDuplicateTypes()
        {
            List<State.Type> list = new List<State.Type>(types.Count);
            foreach (State.Type type in types)
                if (null == (System.Type)type)
                    list.Add(type);
                else
                    list.AddUnique(type);
            types = list;
        }

        public void AddEmptyType()
        {
            types.Add(new State.Type());
        }

        public void Add<T>() where T : ST8.State
        {
            ST8.State.Type type = new ST8.State.Type();
            type.assemblyQualifiedName = typeof(T).AssemblyQualifiedName;

            if (null == types.SingleOrDefault(s => s.assemblyQualifiedName == type.assemblyQualifiedName))
                types.Add(type);
        }

        Coroutine changeStateEoFCoroutine { get; set; }
#if NET_4_6
            = null;
#endif // NET_4_6
        WaitForEndOfFrame wait = new WaitForEndOfFrame();

        public void ChangeEoF<T>() where T : State { ChangeEoF(typeof(T).AssemblyQualifiedName.GetHashCode()); }
        public void ChangeEoF(int key)
        {
            if (null != changeStateEoFCoroutine)
                StopCoroutine(changeStateEoFCoroutine);
            changeStateEoFCoroutine = StartCoroutine(ChangeEoFCoroutine(key));
        }
        IEnumerator ChangeEoFCoroutine(int key)
        {
            yield return wait;

            Change(key);
        }

        public void Change<T>() where T : State { Change(typeof(T).AssemblyQualifiedName.GetHashCode()); }
        public void Change(int key)
        {
            try
            {
                if (playing)
                {
                    if (null != current)
                        current.ExitDelegate();

                    State state = States[key];
                    state.EnterDelegate();

                    current = state;
                }
            }
            catch (Exception exception) { Debug.Log(exception.Message, this); }
        }

        public bool play { get { return playing; } set { playing = value; } }
        public bool pause { get { return !playing; } set { playing = !value; } }
        [HideInInspector] [SerializeField] bool playing = true;
    }
}
