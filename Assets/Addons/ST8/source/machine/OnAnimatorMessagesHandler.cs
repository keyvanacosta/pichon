﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ST8
{
    public abstract partial class Machine : MonoBehaviour
    {
        [DisallowMultipleComponent]
        public class OnAnimatorMessagesHandler : MessageHandler
        {
            void OnAnimatorIK(int layerIndex) { if (machine.playing && null != machine.current) machine.current.OnAnimatorIKDelegate(layerIndex); }

            void OnAnimatorMove() { if (machine.playing && null != machine.current) machine.current.OnAnimatorMoveDelegate(); }

        }
    }
}
