﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ST8
{
    public abstract partial class Machine : MonoBehaviour
    {
        [DisallowMultipleComponent]
        public class LateUpdateMessageHandler : MessageHandler
        {
            void LateUpdate() { if (machine.playing && null != machine.current) machine.current.LateUpdateDelegate(); }
        }
    }
}
