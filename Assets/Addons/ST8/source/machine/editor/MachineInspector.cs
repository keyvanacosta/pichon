﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace ST8.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Machine), true)]
    public class MachineInspector : UnityEditor.Editor
    {
        Machine machine { get { return (Machine)serializedObject.targetObject; } }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            SerializedProperty property = serializedObject.GetIterator();
            property.NextVisible(true);

            do
            {
                switch (property.name)
                {
                    case "m_Script":
                        {
                            EditorGUILayout.PropertyField(property);
                            EditorGUILayout.Separator();

                            float width = EditorGUIUtility.currentViewWidth / 2 - 20;
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.Space(8);
                            if (machine.pause)
                            {
                                if (GUILayout.Button("Play", Utility.ToolbarButton, GUILayout.Width(width)))
                                {
                                    serializedObject.FindProperty("playing").boolValue = true;
                                }
                                GUILayout.Space(4);
                                GUILayout.Button("Paused", Utility.DepressedToolbarButton, GUILayout.Width(width));
                            }
                            else
                            {
                                GUI.backgroundColor = Color.green;
                                GUILayout.Button("Playing", Utility.DepressedToolbarButton, GUILayout.Width(width));
                                GUI.backgroundColor = Color.white;
                                GUILayout.Space(4);
                                if (GUILayout.Button("Pause", Utility.ToolbarButton, GUILayout.Width(width)))
                                {
                                    serializedObject.FindProperty("playing").boolValue = false;
                                }
                            }
                            GUILayout.Space(8);
                            EditorGUILayout.EndHorizontal();

                            EditorGUILayout.Separator();
                        }
                        break;
                    case "types":
                        {
                            SerializedProperty initialProperty = serializedObject.FindProperty("initial");
                            if (serializedObject.isEditingMultipleObjects)
                            {
                                EditorGUILayout.LabelField("Initial state", "Cannot edit multiple objects.");
                            }
                            else
                            {
                                Utility.State.Type.Popup(initialProperty, new GUIContent("Initial state", initialProperty.tooltip), machine.Types);
                            }

                            EditorGUI.BeginChangeCheck();
                                types.DoLayoutList();
                            if (EditorGUI.EndChangeCheck())
                            {
                                serializedObject.ApplyModifiedProperties();
                                machine.RemoveDuplicateTypes();
                                serializedObject.Update();

                                if (!serializedObject.isEditingMultipleObjects)
                                {
                                    SerializedProperty assemblyQualifiedNameProperty = Utility.State.Type.AssemblyQualifiedName(initialProperty);
                                    string assemblyQualifiedName = assemblyQualifiedNameProperty.stringValue;
                                    if (!string.IsNullOrEmpty(assemblyQualifiedName))
                                    {
                                        ST8.State.Type type = machine.Types.SingleOrDefault(T => T.assemblyQualifiedName == assemblyQualifiedName);
                                        if (null == type)
                                        {
                                            assemblyQualifiedNameProperty.stringValue = string.Empty;
                                            serializedObject.ApplyModifiedProperties();
                                            serializedObject.Update();
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    default: EditorGUILayout.PropertyField(property, true); break;
                }
            } while (property.NextVisible(false));

            serializedObject.ApplyModifiedProperties();
        }

        void OnEnable()
        {
            types = new ReorderableList(serializedObject, serializedObject.FindProperty("types"), true, true, true, true);
            types.drawHeaderCallback = TypesHeaderCallback;
            types.drawElementCallback = TypesElementCallback;
            types.onAddCallback = TypesAddCallback;
        }

        ReorderableList types;
        void TypesHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, "States");
        }

        void TypesElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            SerializedProperty type = types.serializedProperty.GetArrayElementAtIndex(index);
            Utility.State.Type.Popup(rect, type, GUIContent.none);
        }
        void TypesAddCallback(ReorderableList list)
        {
            machine.AddEmptyType();
        }
    }
}
