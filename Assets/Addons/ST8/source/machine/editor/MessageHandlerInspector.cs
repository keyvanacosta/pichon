﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace ST8.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Machine.MessageHandler), true)]
    public class MessageHandlerInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI() {}
    }
}
