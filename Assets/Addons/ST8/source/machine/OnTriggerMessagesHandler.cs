﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ST8
{
    public abstract partial class Machine : MonoBehaviour
    {
        [DisallowMultipleComponent]
        public class OnTriggerMessagesHandler : MessageHandler
        {
            void OnTriggerEnter(Collider collider) { if (machine.playing && null != machine.current) machine.current.OnTriggerEnterDelegate(collider); }
            void OnTriggerStay(Collider collider) { if (machine.playing && null != machine.current) machine.current.OnTriggerStayDelegate(collider); }
            void OnTriggerExit(Collider collider) { if (machine.playing && null != machine.current) machine.current.OnTriggerExitDelegate(collider); }
        }
    }
}
