﻿using System;
using System.Runtime.CompilerServices;

using UnityEngine;

namespace ST8
{
    public abstract partial class Machine : MonoBehaviour
    {
        [RequireComponent(typeof(Machine))]
        public abstract class MessageHandler : MonoBehaviour
        {
            public Machine machine
            {
#if NET_4_6
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif // NET_4_6
                get { if (null == machineComponent) machineComponent = GetComponent<Machine>(); return machineComponent; }
            }
            public Machine machineComponent = null;
        }
    }
}
