﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ST8
{
    public abstract partial class Machine : MonoBehaviour
    {
        [DisallowMultipleComponent]
        public class OnCollisionMessagesHandler : MessageHandler
        {
            void OnCollisionEnter(Collision collision) { if (machine.playing && null != machine.current) machine.current.OnCollisionEnterDelegate(collision); }
            void OnCollisionStay(Collision collision) { if (machine.playing && null != machine.current) machine.current.OnCollisionStayDelegate(collision); }
            void OnCollisionExit(Collision collision) { if (machine.playing && null != machine.current) machine.current.OnCollisionExitDelegate(collision); }
        }
    }
}
