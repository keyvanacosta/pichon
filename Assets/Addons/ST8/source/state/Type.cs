﻿using System;
using System.Runtime.CompilerServices;

using UnityEngine;

namespace ST8
{
    public abstract partial class State
    {
        [Serializable]
        public class Type
        {
            public static implicit operator System.Type(Type item)
            {
                if (null == item.type && !string.IsNullOrEmpty(item.assemblyQualifiedName))
                {
                    item.type = System.Type.GetType(item.assemblyQualifiedName);
                }

                return item.type;
            }
            [NonSerialized] System.Type type = null;

            public int key
            {
#if NET_4_6
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif // NET_4_6
                get
                {
                    try
                    {
                        nullableKey = assemblyQualifiedName.GetHashCode();

                        return nullableKey.Value;
                    }
                    catch { throw new UnassignedReferenceException("State.Type has not been initialized!"); }
                }
            }
            int? nullableKey = null;

            public string assemblyQualifiedName;
        }
    }
}
