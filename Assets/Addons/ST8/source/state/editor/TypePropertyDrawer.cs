﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace ST8.Editor
{
    [CustomPropertyDrawer(typeof(State.Type), true)]
    public class TypeProperty : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Utility.State.Type.Popup(position, property, label);
        }
    }
}
