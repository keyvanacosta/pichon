﻿using System;
using System.Runtime.CompilerServices;

using UnityEngine;

namespace ST8
{
    [Serializable]
    public abstract partial class State
    {
        void @delegate(Action action) { if (null != action) action.Invoke(); }
        void @delegate<T>(Action<T> action, T arg) { if (null != action) action.Invoke(arg); }

#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif // NET_4_6
        protected T As<T>() where T : Machine { return (T)machine; }
        internal Machine machine;

#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif // NET_4_6
        protected void ChangeState<S>() where S : State
        {
            machine.Change<S>();
        }


        public int key
        {
#if NET_4_6
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif // NET_4_6
            get { if (null == nullableKey) nullableKey = GetType().AssemblyQualifiedName.GetHashCode(); return nullableKey.Value; }
        }
        int? nullableKey = null;

        protected internal void EnterDelegate() { @delegate(enterDelegate); }
        protected internal void ExitDelegate() { @delegate(exitDelegate); }

        protected internal void UpdateDelegate()         { @delegate(updateDelegate); }
        protected internal void FixedUpdateDelegate()    { @delegate(fixedUpdateDelegate); }

        public bool hasLateUpdate { get { return null != lateUpdateDelegate; } }
        protected internal void LateUpdateDelegate()     { @delegate(lateUpdateDelegate); }

        public bool hasOnCollision { get { return hasOnCollisionEnter || hasOnCollisionStay || hasOnCollisionExit; } }
        public bool hasOnCollisionEnter { get { return null != onCollisionEnterDelegate; } }
        public bool hasOnCollisionStay { get { return null != onCollisionStayDelegate; } }
        public bool hasOnCollisionExit { get { return null != onCollisionExitDelegate; } }

        protected internal void OnCollisionEnterDelegate(Collision collision) { @delegate(onCollisionEnterDelegate, collision); }
        protected internal void OnCollisionStayDelegate(Collision collision) { @delegate(onCollisionStayDelegate, collision); }
        protected internal void OnCollisionExitDelegate(Collision collision) { @delegate(onCollisionExitDelegate, collision); }

        public bool hasOnTrigger { get { return hasOnTriggerEnter || hasOnTriggerStay || hasOnTriggerExit; } }
        public bool hasOnTriggerEnter { get { return null != onTriggerEnterDelegate; } }
        public bool hasOnTriggerStay { get { return null != onTriggerStayDelegate; } }
        public bool hasOnTriggerExit { get { return null != onTriggerExitDelegate; } }

        protected internal void OnTriggerEnterDelegate(Collider collider) { @delegate(onTriggerEnterDelegate, collider); }
        protected internal void OnTriggerStayDelegate(Collider collider) { @delegate(onTriggerStayDelegate, collider); }
        protected internal void OnTriggerExitDelegate(Collider collider) { @delegate(onTriggerExitDelegate, collider); }

        public bool hasOnAnimator { get { return hasOnAnimatorIK || hasOnAnimatorMove; } }
        public bool hasOnAnimatorIK { get { return null != onAnimatorIKDelegate; } }
        public bool hasOnAnimatorMove { get { return null != onAnimatorMoveDelegate; } }
        protected internal void OnAnimatorIKDelegate(int layerIndex) { @delegate(onAnimatorIKDelegate, layerIndex); }
        protected internal void OnAnimatorMoveDelegate() { @delegate(onAnimatorMoveDelegate); }

        public State()
        {
            System.Type[] emptyTypeArray = new System.Type[0];

            enterDelegate = this.GetDelegate<Action>("Enter", types: emptyTypeArray);
            exitDelegate = this.GetDelegate<Action>("Exit", types: emptyTypeArray);

            updateDelegate = this.GetDelegate<Action>("Update", types: emptyTypeArray);
            fixedUpdateDelegate = this.GetDelegate<Action>("FixedUpdate", types: emptyTypeArray);
            lateUpdateDelegate = this.GetDelegate<Action>("LateUpdate", types: emptyTypeArray);

            System.Type[] collisionTypeArray = new System.Type[1] { typeof(Collision) };

            onCollisionEnterDelegate = this.GetDelegate<Action<Collision>>("OnCollisionEnter", types: collisionTypeArray);
            onCollisionStayDelegate = this.GetDelegate<Action<Collision>>("OnCollisionStay", types: collisionTypeArray);
            onCollisionExitDelegate = this.GetDelegate<Action<Collision>>("OnCollisionExit", types: collisionTypeArray);

            System.Type[] colliderTypeArray = new System.Type[1] { typeof(Collider) };

            onTriggerEnterDelegate = this.GetDelegate<Action<Collider>>("OnTriggerEnter", types: colliderTypeArray);
            onTriggerStayDelegate = this.GetDelegate<Action<Collider>>("OnTriggerStay", types: colliderTypeArray);
            onTriggerExitDelegate = this.GetDelegate<Action<Collider>>("OnTriggerExit", types: colliderTypeArray);

            System.Type[] intTypeArray = new System.Type[1] { typeof(int) };

            onAnimatorIKDelegate = this.GetDelegate<Action<int>>("OnAnimatorIK", types: intTypeArray);
            onAnimatorMoveDelegate = this.GetDelegate<Action>("OnAnimatorMove", types: emptyTypeArray);
        }
        
        Action enterDelegate = null;
        Action exitDelegate = null;

        Action updateDelegate = null;
        Action fixedUpdateDelegate = null;

        Action lateUpdateDelegate = null;

        Action<Collision> onCollisionEnterDelegate = null;
        Action<Collision> onCollisionStayDelegate = null;
        Action<Collision> onCollisionExitDelegate = null;

        Action<Collider> onTriggerEnterDelegate = null;
        Action<Collider> onTriggerStayDelegate = null;
        Action<Collider> onTriggerExitDelegate = null;

        Action<int> onAnimatorIKDelegate = null;
        Action onAnimatorMoveDelegate = null;
    }
}
