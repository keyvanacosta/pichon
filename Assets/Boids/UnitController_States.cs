﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ST8;

public partial class UnitController : Machine
{
    public class Compute : State
    {

        //void Enter()
        //{

        //}

        void Update()
        {

            UnitController ucBoids = As<UnitController>();

            ucBoids.myCenter = Vector3.zero;
            ucBoids.myVelocity = Vector3.zero;
            Boid_Brains b = null;
            foreach (GameObject g in ucBoids.boidList)
            {
                Vector3 applyVector = Vector3.zero;
                b = g.GetComponent<Boid_Brains>();

                if (b.doCenter)
                {
                    applyVector = ucBoids.UpdateCenter(g);
                    b.center.x = (applyVector.x - b.myTransform.position.x) * b.centerDesire;
                    b.center.y = (applyVector.y - b.myTransform.position.y) * b.centerDesire;
                    b.center.z = (applyVector.z - b.myTransform.position.z) * b.centerDesire;
                }
                if (b.doVelocity)
                {
                    applyVector = ucBoids.UpdateVelocity(g);
                    b.velocity.x = applyVector.x * b.groupSpeed;
                    b.velocity.y = applyVector.y * b.groupSpeed;
                    b.velocity.z = applyVector.z * b.groupSpeed;
                }

                if (b.doDistance)
                {
                    applyVector = ucBoids.UpdateDistance(g, b.distanceMag);

                    b.distance.x = applyVector.x * b.groupDesire;
                    b.distance.y = applyVector.y * b.groupDesire;
                    b.distance.z = applyVector.z * b.groupDesire;

                }

                if (b.targetObject != null)
                {
                    b.center.x = (b.targetObject.gameObject.transform.position.x - g.transform.position.x) * b.targetdesire;
                    b.center.y = (b.targetObject.gameObject.transform.position.y - g.transform.position.y) * b.targetdesire;
                    b.center.z = (b.targetObject.gameObject.transform.position.z - g.transform.position.z) * b.targetdesire;
                }

                ucBoids.myVelocity /= ucBoids.boidList.Count;
                ucBoids.myCenter /= ucBoids.boidList.Count;
            }
        }

        void Exit()
        {

        }

    }
}