﻿using UnityEngine;
using ST8;

public class Gnat_State_Hover : State
{

    // Use this for initialization
    //void Enter()
    //{

    //}

    void FixedUpdate()
    {

        Boid_Brains brains = As<Boid_Brains>();

        //brains.rbRef.AddForce(brains.center);
        //brains.rbRef.AddForce(brains.myVelocity);
        //brains.rbRef.AddForce(brains.distance);
        //brains.rbRef.AddForce(brains.velocity);

        brains.rbRef.velocity = Vector3.zero;

    }


    // Update is called once per frame
    void Update()
    {

        Boid_Brains brains = As<Boid_Brains>();

        brains.position = brains.gameObject.transform.position;
        brains.myTransform = brains.transform;

    }

    void Exit()
    {

    }
}
