﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ST8;

public partial class UnitController : Machine
{

    public bool displayCenter = true;
    public Vector3 myCenter;
    public Vector3 myVelocity;
    public GameObject superBoidPrefab;
    public GameObject boidPrefab;

    public static bool initialized = false;

    public static bool initializedIsTrue()
    {
        return initialized == true;
    }

    public List<GameObject> boidList = new List<GameObject>();

    public Dictionary<GameObject, Boid_Brains> boidDictionary = new Dictionary<GameObject, Boid_Brains>();

    private static UnitController _instance;

    public static UnitController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UnitController>();

            }
            return _instance;
        }
    }

    public override void Awake()
    {

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {

            base.Awake();
            _instance = this;
            if (initialized != true)
            {
                initializeArrays();
            }

        }
    }

    public void initializeArrays()
    {
        
        boidList = new List<GameObject>();
        boidList.Clear();

        boidDictionary = new Dictionary<GameObject, Boid_Brains>();
        boidDictionary.Clear();

        initialized = true;
        
    }

    public override void Start()
    {

        

        base.Start();

        GameObject me = Instantiate(superBoidPrefab);
        me.transform.parent = gameObject.transform;
        me.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.red;

    }

    public void registerBoid(GameObject newBoid)
    {
        boidList.Add(newBoid);
        boidDictionary.Add(newBoid, newBoid.GetComponent<Boid_Brains>());
    }

    public void unregisterBoid(GameObject newBoid)
    {
        boidList.Remove(newBoid);
        boidDictionary.Remove(newBoid);
    }

    public override void FixedUpdate()
    {
        transform.position = myCenter;
    }

    public Vector3 UpdateCenter(GameObject _boid)
    {

        Vector3 center = Vector3.zero;

        foreach (GameObject g in boidList)
        {

            if (g == _boid)
            {
                continue;
            }

            center += g.transform.position;

        }

        myCenter += center;

        if (boidList.Count > 1)
        {
            center.x /= boidList.Count - 1;
            center.y /= boidList.Count - 1;
            center.z /= boidList.Count - 1;
        }


        return center;

    }

    public Vector3 UpdateVelocity(GameObject _boid)
    {

        Vector3 velocity = Vector3.zero;

        foreach (GameObject g in boidList)
        {

            if (g == _boid)
            {
                continue;
            }

            velocity += boidDictionary[g].velocity;

        }

        myVelocity += velocity;

        if (boidList.Count > 1)
        {
            velocity.x /= boidList.Count - 1;
            velocity.y /= boidList.Count - 1;
            velocity.z /= boidList.Count - 1;
        }

        return velocity;
    }



    public Vector3 UpdateDistance(GameObject _boid, float zoneDistance)
    {

        Vector3 distance = Vector3.zero;
        Vector3 vectorCorrection = Vector3.zero;
        //int currentBoid = 0;
        //float currentDistance = 0.0f;
        //Boid_Brains b = _boid.GetComponent<Boid_Brains>();

        foreach (GameObject g in boidList)
        {

            if (g != _boid)
            {
                distance = (g.transform.position - _boid.transform.position);
                if (distance.magnitude < zoneDistance)
                {
                    vectorCorrection -= distance;
                }
            }

        }
        return vectorCorrection;

    }


    // Update is called once per frame
    public override void Update()
    {

        base.Update();
        
    }
}