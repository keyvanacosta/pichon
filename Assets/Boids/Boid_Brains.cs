﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ST8;

// Boid Brains:
//  A Boids flocking example, implementation by Keyvan Acosta
//  August 30th, 2018
// Help for Singleton provided by the following link:
//https://gamedev.stackexchange.com/questions/116009/in-unity-how-do-i-correctly-implement-the-singleton-pattern
// Help for waitUntil provided by the following link:
//https://www.reddit.com/r/Unity3D/comments/4lhj6k/confused_by_waituntil_help/
// Boid Technical design provided by the following link:
// http://www.kfish.org/boids/pseudocode.html

[RequireComponent(typeof(Rigidbody))]
public class Boid_Brains : Machine
{

    public Rigidbody rbRef;

    //public GameObject MainController;
    public static UnitController MainBoid;
    public Transform myTransform;
    public Vector3 distance;
    public Vector3 center;
    public Vector3 position;
    public Vector3 myVelocity;
    public Vector3 velocity;
    public Vector3 direction;
    public Vector3 proximity;
    public Vector3 separation;
    public Vector3 avoidance;

    public float targetdesire = 0.1f;
    public float centerDesire = 0.1f;
    public float groupDesire = 0.1f;
    public float groupSpeed = 0.1f;
    public float distanceMag = 10;

    public bool doCenter = true;
    public bool doVelocity = true;
    public bool doDistance = true;

    public GameObject targetObject;

    public int state;

    public int id;

    public int follow = -1;

    public override void Awake()
    {

        base.Awake();

        MainBoid = UnitController.Instance;

        id = GetHashCode();

    }

    public IEnumerator regMe()
    {

        //yield return new WaitUntil(() => UnitController.initialized == true);

        yield return new WaitUntil(UnitController.initializedIsTrue);
        Debug.Log("Register Me:" + gameObject.name);
        UnitController.Instance.registerBoid(gameObject);

    }

    public void OnDestroy()
    {
        Debug.Log("destroying");
        if (UnitController.Instance != null)
            UnitController.Instance.unregisterBoid(gameObject);

    }

    public override void FixedUpdate()
    {

        base.FixedUpdate();

        //    rbRef.AddForce(center);
        //    rbRef.AddForce(myVelocity);
        //    rbRef.AddForce(distance);
        //    rbRef.AddForce(velocity);

        //    rbRef.velocity = Vector3.ClampMagnitude(rbRef.velocity, 1);//myVelocity.magnitude);

    }

    public override void Start()
    {

        base.Start();

        rbRef = gameObject.GetComponent<Rigidbody>();
        myVelocity = Vector3.zero;
        if (doVelocity)
        {
            myVelocity = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
        }
        velocity = myVelocity;
        StartCoroutine(regMe());

    }

    public override void Update()
    {

        base.Update();

        //position = gameObject.transform.position;
        //myTransform = transform;

    }

}


/*
 * public class Boid_Brains : MonoBehaviour
{

    public Rigidbody rbRef;

    //public GameObject MainController;
    public static UnitController MainBoid;
    public Transform myTransform;
    public Vector3 distance;
    public Vector3 center;
    public Vector3 position;
    public Vector3 myVelocity;
    public Vector3 velocity;
    public Vector3 direction;
    public Vector3 proximity;
    public Vector3 separation;
    public Vector3 avoidance;

    public float targetdesire = 0.1f;
    public float centerDesire = 0.1f;
    public float groupDesire = 0.1f;
    public float groupSpeed = 0.1f;
    public float distanceMag = 10;

    public bool doCenter = true;
    public bool doVelocity = true;
    public bool doDistance = true;

    public GameObject targetObject;

    public int state;

    public int id;

    public int follow = -1;

    private void Awake()
    {
        MainBoid = UnitController.Instance;

        id = GetHashCode();
    }

    public void RuleCenterMass()
    {

    }
    public void RuleKeepDistance()
    {

    }
    public void RuleMatchVelocity()
    {

    }

    public IEnumerator regMe()
    {

        //yield return new WaitUntil(() => UnitController.initialized == true);

        yield return new WaitUntil(UnitController.initializedIsTrue);
        Debug.Log("Register Me:" + gameObject.name);
        UnitController.Instance.registerBoid(gameObject);

    }

    private void OnDestroy()
    {
        Debug.Log("destroying");
        if (UnitController.Instance != null)
            UnitController.Instance.unregisterBoid(gameObject);

    }

    private void FixedUpdate()
    {

        rbRef.AddForce(center);
        rbRef.AddForce(myVelocity);
        rbRef.AddForce(distance);
        rbRef.AddForce(velocity);

        rbRef.velocity = Vector3.ClampMagnitude(rbRef.velocity, 1);//myVelocity.magnitude);

    }

    private void Start()
    {
        rbRef = gameObject.GetComponent<Rigidbody>();
        myVelocity = Vector3.zero;
        if (doVelocity)
        {
            myVelocity = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
        }
        velocity = myVelocity;
        StartCoroutine(regMe());
    }

    private void Update()
    {

        position = gameObject.transform.position;
        myTransform = transform;


    }

}
*/