﻿using UnityEngine;

public class WindZone : MonoBehaviour
{
    public float windSpeed;

    private void OnTriggerEnter(Collider other)
    {
        //other.attachedRigidbody.AddForce(transform.right * windSpeed);
        //other.attachedRigidbody.velocity += transform.right * windSpeed;
        HBird bird = other.GetComponentInParent<HBird>();
        if (bird != null)
        {
            Debug.Log("Push Bird");
            bird.externalForces += transform.right * windSpeed;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //other.attachedRigidbody.AddForce(transform.right * windSpeed);
        //other.attachedRigidbody.velocity += transform.right * windSpeed;
        HBird bird = other.GetComponentInParent<HBird>();
        if (bird != null)
        {
            Debug.Log("Stop Push Bird");
            bird.externalForces -= transform.right * windSpeed;
        }
    }
}
