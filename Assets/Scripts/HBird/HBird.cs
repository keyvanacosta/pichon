﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ST8;

public class HBird : Machine
{
    public float _speed = 1;
    public float speed
    {
        get { return _speed; }
        set
        {
            _speed = value;
            moveSpeed = _speed * 10;
        }
    }

    public float _strength;
    public float strength
    {
        get { return _strength; }
        set
        {
            _strength = value;
            energyLossRate = 100 / _strength * 0.25f;
            maxElevation = _strength * 5f;
        }
    }

    public float _maxEnergy = 1;
    public float maxEnergy
    {
        get { return _maxEnergy; }
        set
        {
            _maxEnergy = value;
            float midPoint = _maxEnergy * 0.5f;
            lowerEnergyThreshold = midPoint - speed;
            upperEnergyThreshold = midPoint + speed;
        }
    }



    [Space(15)]
    public float currentEnergy;
    public float lowerEnergyThreshold;
    public float upperEnergyThreshold;
    public float energyLossRate = 1.0f;
    public float energy
    {
        get { return currentEnergy; }
        set
        {
            currentEnergy = value;
            if (currentEnergy < 0) { currentEnergy = 0; }

            //energyMod = 1;
            if (currentEnergy > lowerEnergyThreshold && currentEnergy < upperEnergyThreshold)
            {
                //energyMod = 2;
            }
            else if (currentEnergy < lowerEnergyThreshold)
            {
                //energyMod = 0.5f;
            }
        }
    }

    public float maxElevation = 30;
    public float moveSpeed = 10;
    public float decelerationRate = 0.5f;
    //float energyMod = 1;


    public LayerMask targetingLayers;
    public float targetingRadius = 5;

    [HideInInspector] public Vector3 externalForces = Vector3.zero;
    [HideInInspector] public Vector3 faceDir = Vector3.right;
    //[HideInInspector] public Vector3 axisDir = Vector3.zero;
    [HideInInspector] public Vector3 moveDir;
    [HideInInspector] public Vector3 axisDir;
    [HideInInspector] public Vector3 orbitPos;

    [HideInInspector] public float leanBlend;
    [HideInInspector] public bool isMoving;
    [HideInInspector] public bool isTargeting;

    public Transform skelMesh;
    public BeakTrigger beakTrigger;
    public Slider bar;

    //[HideInInspector]
    public Transform target;
    [HideInInspector] public Animator anim;
    [HideInInspector] public Rigidbody rb;

    [HideInInspector] public int horizontalLeanID;
    [HideInInspector] public int verticalLeanID;
    [HideInInspector] public int sideLeanID;
    [HideInInspector] public int targetingID;


    public Vector3 position { get { return rb.position; } set { rb.position = value; } }
    public Vector3 velocity { get { return rb.velocity; } set { rb.velocity = value; } }

    public override void Awake()
    {
        Add<HBirdState_Base>();
        Add<HBirdState_Targeting>();
        Initial<HBirdState_Base>();

        base.Awake();
    }

    public override void Start()
    {
        base.Start();


        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        horizontalLeanID = Animator.StringToHash("horizontalLean");
        verticalLeanID = Animator.StringToHash("verticalLean");
        sideLeanID = Animator.StringToHash("sideLean");
        targetingID = Animator.StringToHash("targeting");

        faceDir = Vector3.right;

        strength = _strength;
        speed = _speed;
        maxEnergy = _maxEnergy;


        currentEnergy = maxEnergy;
        bar.maxValue = maxEnergy;

        beakTrigger.gameObject.SetActive(false);
    }


    public override void Update()
    {
        base.Update();

        isMoving = moveDir.sqrMagnitude > 0.1f;


        UpdateEnergy();

        bar.value = energy; 
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        rb.MoveRotation(Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(faceDir, Vector3.up), Time.deltaTime * 20));
    }

    void UpdateEnergy()
    {
        energy -= Time.deltaTime * energyLossRate;
    }




    public void OnBeakHit(Collider col)
    {
        Debug.Log(col.name);

        FlowerStem flower = col.transform.root.GetComponent<FlowerStem>();
        if(flower != null)
        {
            Vector3 targetVector = transform.position - target.position;
            float dot = Vector3.Dot(targetVector.normalized, target.forward);
            Debug.Log(col.name + " | " + dot);

            if (dot > 0)
            {
                energy += flower.energyValue * dot;
                flower.OnBirdConsumed();
            }
        }
    }





    void OnDrawGizmos()
    {
        Color c = Color.blue;
        c.a = 0.25f;
        Gizmos.color = c;
        Gizmos.DrawWireSphere(transform.position, targetingRadius);
    }
}
