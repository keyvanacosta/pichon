﻿using UnityEngine;
using System.Collections;
using ST8;

public class HBirdState_Targeting : State
{
    HBird bird;
    public SphericalCoordinates sc;
    private Vector3 targetVector;


    public void Enter()
    {
        Debug.Log("Enter targeting");
        if (bird == null) { bird = As<HBird>(); }

        bird.anim.SetBool(bird.targetingID, true);

        if (bird.target != null)
        {
            targetVector = bird.transform.position - bird.target.position;

            if (sc == null)
            {
                sc = new SphericalCoordinates(targetVector, 0.5f, 5f, 0f, Mathf.PI * 2f, -(Mathf.PI / 3.5f), Mathf.PI / 3f);
            }
            else
            {
                sc.FromCartesian(targetVector);
            }
            bird.orbitPos = bird.target.position + sc.toCartesian;


            Viewer.instance.focalRadius = 4;
            Viewer.instance.hardMoveDistance = 6;
        }
    }

    public void FixedUpdate()
    {
        if (!bird.isTargeting || HasLostTarget()) { ChangeState<HBirdState_Base>(); }


        if (bird.target)
        {
            float orbitSpeed = bird.speed * 0.5f;

            bird.anim.SetFloat(bird.horizontalLeanID, bird.axisDir.y);
            bird.anim.SetFloat(bird.sideLeanID, bird.moveDir.x);

            bird.beakTrigger.gameObject.SetActive(bird.axisDir.y >= 0.5f);

            targetVector = bird.target.position - bird.transform.position;
            bird.faceDir = targetVector;

            if (bird.isMoving)
            {
                bird.orbitPos = bird.target.position + sc.Rotate( bird.moveDir.x * orbitSpeed * Time.deltaTime, bird.moveDir.y * orbitSpeed * Time.deltaTime).toCartesian;
            }

            if (Mathf.Abs(bird.axisDir.y) > 0.01f)
            {
                bird.orbitPos = bird.target.position + sc.TranslateRadius(-bird.axisDir.y * orbitSpeed * Time.deltaTime).toCartesian;
            }

            bird.rb.MovePosition(bird.orbitPos);
        }
        else
        {
            bird.rb.velocity = Vector3.zero;
        }
    }

    public void Exit()
    {
        Debug.Log("Exit targeting");
        Vector3 dir = bird.faceDir;
        dir.y = 0;
        dir.z = 0;
        bird.faceDir = dir.normalized;
        bird.anim.SetBool(bird.targetingID, false);
        bird.beakTrigger.gameObject.SetActive(false);
        bird.target = null;

        Viewer.instance.focalRadius = 2;
        Viewer.instance.hardMoveDistance = 4;
    }

    bool HasLostTarget()
    {
        return false;//context.target == null || !context.target.gameObject.activeInHierarchy;
    }
}
