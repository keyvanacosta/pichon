﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeakTrigger : MonoBehaviour
{
    public HBird hBird;

    private void OnTriggerEnter(Collider col)
    {
        hBird.OnBeakHit(col);
    }

    //private void OnTriggerExit(Collider col)
    //{
    //    hBird.OnBeakHit(col);
    //}
}
