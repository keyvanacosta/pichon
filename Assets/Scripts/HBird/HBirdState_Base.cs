﻿using UnityEngine;
using System.Collections;
using ST8;

public class HBirdState_Base : State
{
    bool hasCheckedForTarget = false;
    HBird bird;

    void Enter()
    {
        Debug.Log(nameof(HBirdState_Base));

        if (bird == null) { bird = As<HBird>(); }
    }

    void FixedUpdate()
    {
        if (bird.isTargeting)
        {
            if (!hasCheckedForTarget && IsValidTarget()) { ChangeState<HBirdState_Targeting>(); }
        }
        else
        {
            hasCheckedForTarget = false;
        }


        // make it fly!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        float faceDot = Vector3.Dot(bird.faceDir, bird.velocity.normalized);
        if (bird.isMoving)
        {
            bird.velocity =  Vector3.ClampMagnitude(bird.moveDir, 1) * bird.speed;
            if(faceDot < -0.8f && bird.velocity.magnitude > (bird.speed * 0.9f)) { bird.faceDir *= -1; }
        }
        else
        {
            bird.velocity *= bird.decelerationRate;
        }

        //Debug.DrawRay(bird.position, bird.externalForces, Color.yellow);
        // apply external forces if any
        bird.velocity += bird.externalForces;


        bird.leanBlend = bird.velocity.magnitude / bird.speed * Mathf.Sign(faceDot);
        bird.anim.SetFloat(bird.horizontalLeanID, bird.leanBlend);
        bird.anim.SetFloat(bird.verticalLeanID, Vector3.ClampMagnitude(bird.velocity, 1).y);

        // we do not rigidly clamp to 2D plane
        // when bird gets knoked out, smooth back into 2D plane
        if (!Mathf.Approximately(bird.position.z, 0))
        {
            bird.velocity += new Vector3(0,0,-bird.position.z * 0.5f);
        }
        
    }

    void Exit() { }

    bool IsValidTarget()
    {
        //Debug.Log("Cheking for targets");
        hasCheckedForTarget = true;
        Collider[] cols = Physics.OverlapSphere(bird.transform.position, bird.targetingRadius, bird.targetingLayers);
        if (cols.Length > 0)
        {
            //Debug.Log("Found target " + cols[0].name);
            bird.target = cols[0].transform;
            return true;
        }

        return false;
    }
}
