﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshPlacer : MonoBehaviour
{


    public List<Vector3> newVertices;
    public List<Vector3> newNormals;
    public Vector2[] newUV;
    public int[] newTriangles;
    Mesh mesh;// = new Mesh();
    MeshCollider mCollider;

    public GameObject objectToDot;

    private void Awake()
    {
        mesh = new Mesh();
        mesh = objectToDot.GetComponent<MeshFilter>().mesh;
        newVertices = new List<Vector3>();
        newNormals = new List<Vector3>();
        mesh.MarkDynamic();
        mesh.UploadMeshData(false);
        mesh.GetVertices(newVertices);
        mesh.GetNormals(newNormals);

        //mesh.vertices = newVertices.ToArray();
        //mesh.uv = newUV;
        //mesh.triangles = newTriangles;
        //mesh.MarkDynamic();
        //mesh.UploadMeshData(false);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.vertices = newVertices.ToArray();
        }
    }
}
