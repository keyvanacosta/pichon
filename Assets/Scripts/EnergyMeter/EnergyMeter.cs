﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyMeter : MonoBehaviour
{

    public GameObject biscuit;



    [SerializeField]
    private float meterScale = .1f;

    public int currentStrength = 4;
    public float currentMarker = 10;
    [Space(10)]
    public float energy = 20;
    public float strength = 20;
    public float speed = 20;

    [Space(10)]
    public int spMin = 20;
    public int spMax = 50;
    [Space(5)]
    public int stMin = 30;
    public int stMax = 40;
    [Space(5)]
    public int enMin = 20;
    public int enMax = 40;

    public GameObject locateObject;

    Vector3[] sizes;
    Vector3[] positions;
    List<GameObject> piezos;
    List<GameObject> speedPiezos;
    //List<GameObject> strengthPiezos;
    List<GameObject> energyPiezos;
    List<GameObject> energy1Piezos;
    List<GameObject> energy2Piezos;
    Dictionary<GameObject, piezoMeter> piezoBook;

    private void Awake()
    {

        spMin = (int)(speed * ((float)spMin / 100));
        spMax = (int)(speed * ((float)spMax / 100));

        stMin = (int)(strength * ((float)stMin / 100));
        stMax = (int)(strength * ((float)stMax / 100));

        if (stMin < 1) stMin = 1;

        enMin = (int)(energy * ((float)enMin / 100));
        enMax = (int)(energy * ((float)enMax / 100));

        piezos = new List<GameObject>();
        speedPiezos = new List<GameObject>();
        //strengthPiezos = new List<GameObject>();
        energyPiezos = new List<GameObject>();
        energy1Piezos = new List<GameObject>();
        energy2Piezos = new List<GameObject>();
        piezoBook = new Dictionary<GameObject, piezoMeter>();

        currentStrength = stMin;

        for (int j = (int)strength; j > 0; j--)
        {
            float tempj = (float)j;
            for (int i = 0; i < energy; i++)
            {
                float tempi = (float)i;
                GameObject go = Instantiate(biscuit, new Vector3(tempi * meterScale, tempj * meterScale, 0), Quaternion.identity, gameObject.transform);
                go.transform.localScale = new Vector3(.25f, .25f, .1f);
                //sizes[i] = new Vector3(.25f, .25f, .1f);
                //positions[i] = go.transform.position;

                piezos.Add(go);
                piezoBook.Add(go, go.GetComponent<piezoMeter>());

                int type = 0;

                //if (j <= stMax && j > stMin)
                //{
                //    type |= 1;
                //}
                //if (i >= spMin && i < spMax && currentStrength >= stMax)
                //{
                //    type |= 2;
                //}
                //if (i == (int)currentMarker && j <= stMax && j > stMin)
                //{
                //    type = 5;
                //}
                //if (i > currentMarker && j <= stMax && j > stMin)
                //{
                //    type = 4;
                //}
                //Debug.Log(i.ToString() + ":" + j.ToString() + "=" + type);

                go.GetComponent<piezoMeter>().TagType = type;
                go.GetComponent<piezoMeter>().meter2DIndexes = new Vector3(i, j, 0);

            }
        }

        foreach (KeyValuePair<GameObject, piezoMeter> item in piezoBook)
        {
            item.Value.fixType();
        }
    }

    public void arrange2D()
    {

        foreach (KeyValuePair<GameObject, piezoMeter> item in piezoBook)
        {
            item.Value.setPosition(new Vector3(meterScale * (gameObject.transform.position.x + item.Value.meter2DIndexes.x), meterScale * (gameObject.transform.position.y + item.Value.meter2DIndexes.y), 0));
        }

    }

    public void recalculate()
    {

        int type = 0;
        int i = 0;
        int j = 0;
        energyPiezos.Clear();
        energy2Piezos.Clear();
        speedPiezos.Clear();
        energy1Piezos.Clear();
        foreach (KeyValuePair<GameObject, piezoMeter> item in piezoBook)
        {

            j = (int)item.Value.meter2DIndexes.y;
            i = (int)item.Value.meter2DIndexes.x;
            type = 0;

            /// SHOW ENERGY
            if (i <= energy && j <= currentStrength)
            {
                type |= 1;
            }

            //  Testing                     -------------------
            if (i >= spMin && i <= spMax)
            {
                type |= 2;
            }

            if (i >= spMin && i <= spMax && j > stMin)
            {
                type ^= 2;
            }

            if (i > spMax && j <= currentStrength)
            {
                type = 4;
            }

            if (i == currentMarker && j <= currentStrength)
            {
                type = 5;
            }
            item.Value.TagType = type;
            switch (type)
            {
                case 1:
                    energyPiezos.Add(item.Key);
                    break;
                case 2:
                    energy2Piezos.Add(item.Key);
                    break;
                case 3:
                    speedPiezos.Add(item.Key);
                    break;
                case 4:
                    energy1Piezos.Add(item.Key);
                    break;
            }
            item.Value.fixType();
        }
    }

    void grow()
    {
        //int i = 0;
        foreach (KeyValuePair<GameObject, piezoMeter> item in piezoBook)
        {
            item.Key.transform.Translate(new Vector3(0, meterScale * Mathf.Sin((float)(item.Value.meter2DIndexes.x) * .1f) * (gameObject.transform.position.y + item.Value.meter2DIndexes.y), 0));
        }

    }

    // Use this for initialization
    void Start()
    {

    }

    void displayPiezos()
    {
        //int i = 0;

        if (locateObject == null)
        {
            return;
        }

        int countV = locateObject.GetComponent<MeshPlacer>().newVertices.Count;
        Vector3 pos = locateObject.transform.position;
        int countP = energy1Piezos.Count;
        int j = 0;
        for (int i = 0; i < countV; i++)
        {
            if (j < countP)
            {
                pos.x = pos.x + locateObject.GetComponent<MeshPlacer>().newVertices[i].x;
                pos.y = pos.y + locateObject.GetComponent<MeshPlacer>().newVertices[i].y;
                energy1Piezos[j].transform.position = new Vector3(pos.x, pos.y, 0);
                j++;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //grow();

        if (Input.GetKey(KeyCode.Space))
        {
            arrange2D();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            recalculate();
        }

        if (meterScale < 0.0f)
            meterScale = 0;


        if (meterScale > 2.0f)
            meterScale = 2;

        if (Input.GetKey(KeyCode.A))
        {
            meterScale -= .01f;
            arrange2D();
        }

        if (Input.GetKey(KeyCode.Q))
        {
            meterScale += .01f;
            arrange2D();
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            currentMarker += 1;
            if (currentMarker >= energy)
            {

                currentMarker = energy - 1;

            }
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            currentMarker -= 1;
            if (currentMarker <= 0)
            {
                currentMarker = 0;
            }
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            currentStrength += 1;
            if (currentStrength >= stMax) currentStrength = stMax;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            currentStrength -= 1;
            if (currentStrength <= 1) currentStrength = 1;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            spMin += 1;
            if (spMin >= spMax) spMin = (int)spMax;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            spMin -= 1;
            if (spMin <= 0) spMin = 0;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            spMax += 1;
            if (spMax >= speed) spMax = (int)speed;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            spMax -= 1;
            if (spMax <= spMin) spMax = spMin;
            recalculate();
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            stMin += 1;
            if (stMin >= stMax) stMin = (int)stMax;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            stMin -= 1;
            if (stMin <= 0) stMin = 0;
            recalculate();
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            stMax += 1;
            if (stMax >= strength) stMax = (int)strength;
            recalculate();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            stMax -= 1;
            if (stMax <= stMin) stMax = stMin;
            recalculate();
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            displayPiezos();
        }
    }
}
