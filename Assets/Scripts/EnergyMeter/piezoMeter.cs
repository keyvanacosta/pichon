﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class piezoMeter : MonoBehaviour
{

    public int type = 0;

    public Material defaultMaterial;
    public Material energyMaterial;
    public Material strengthMaterial;
    public Material speedMaterial;
    public Material currentMarkerMaterial;

    public Vector3 meter2DIndexes;

    public int TagType
    {
        get
        {
            return type;
        }
        set
        {
            type = value;
        }
    }

    public float mySize;

    public Vector3 myPosition;

    public bool active = true;



    // Use this for initialization
    void Start()
    {

    }

    private void OnMouseDown()
    {
        type++;
        Debug.Log(type);
        if (type > 5)
        {
            type = 1;
        }
        fixType();
    }

    public void fixType()
    {

        gameObject.GetComponent<Renderer>().enabled = true;

        switch (type)
        {
            case 0:
                gameObject.GetComponent<Renderer>().enabled = false;
                break;
            case 1:
                gameObject.GetComponent<Renderer>().material = defaultMaterial;
                break;
            case 2:
                gameObject.GetComponent<Renderer>().material = strengthMaterial;
                break;
            case 3:
                gameObject.GetComponent<Renderer>().material = speedMaterial;
                break;
            case 4:
                gameObject.GetComponent<Renderer>().material = energyMaterial;
                break;
            case 5:
                gameObject.GetComponent<Renderer>().material = currentMarkerMaterial;
                break;
            default:
                //type = 1;
                break;
        }
    }





    public void setPosition(Vector3 _pos)
    {
        myPosition = _pos;
        transform.position = _pos;
    }


    // Update is called once per frame
    void Update()
    {

    }

    //is there a way of making a function/event based on an event's changing value
    // like a "watch" (debugger)

}
