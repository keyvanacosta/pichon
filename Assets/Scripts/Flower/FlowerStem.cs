﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerStem : MonoBehaviour
{
    public float energyValue = 5;
    public float flowerReturnTime = 5;

    public int stemSegments = 5;
    public float stemSegmentLength = 0.4f;
    public float stemSegmentThickness = 0.25f;
    public float stemSegmentThicknessVariation = 0.1f;
    public float maxRotPerSegment = 5;

    float segmentThickness = 1;

    Transform segment;
    Transform flower;

    void Start()
    {
        segment = transform.Find("StemSegment");
        flower = transform.Find("Flower");

        CreateRandomStem();
    }

    void CreateRandomStem()
    {
        segmentThickness = stemSegmentThickness + (stemSegmentThicknessVariation * stemSegments);

        AdjustSegment(segment, segmentThickness);

        Transform[] segmentList = new Transform[stemSegments];
        segmentList[0] = segment;
        for (int i = 1; i < stemSegments; i++)
        {
            segmentList[i] = Instantiate<Transform>(segment);

            segmentThickness -= stemSegmentThicknessVariation;
            AdjustSegment(segmentList[i], segmentThickness);
        }

        float rotValue = 0;
        for (int i = 1; i < segmentList.Length; i++)
        {
            rotValue = Random.Range(-maxRotPerSegment, maxRotPerSegment);
            segmentList[i].parent = segmentList[i-1];
            segmentList[i].localPosition = new Vector3(0, stemSegmentLength * 2, 0);
            segmentList[i].localEulerAngles = new Vector3(rotValue, 0, rotValue);
        }

        flower.parent = segmentList[stemSegments - 1];
        flower.localPosition = new Vector3(0, 0.8f, 0);
        rotValue = Random.Range(-maxRotPerSegment, maxRotPerSegment);
        flower.localEulerAngles = new Vector3(-90 + rotValue, rotValue, rotValue);
    }

    void AdjustSegment(Transform thisSegment, float thickness)
    {
        // TEMP ////////////////////////////////////////////////
        Transform child = thisSegment.GetChild(0);
        child.localScale = new Vector3(thickness, stemSegmentLength, thickness);
        child.localPosition = new Vector3(0, stemSegmentLength, 0);
        //////////////////////////////////////////////////////////
    }

    public void OnBirdConsumed()
    {
        flower.gameObject.SetActive(false);
        Invoke("FlowerReturn", flowerReturnTime);
    }

    void FlowerReturn()
    {
        flower.gameObject.SetActive(true);
    }
}
