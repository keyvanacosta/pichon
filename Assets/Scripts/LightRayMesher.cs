﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRayMesher : MonoBehaviour {

    // Use this for initialization
    public List<Vector3> newVertices;
    public Vector2[] newUV;
    public int[] newTriangles;
    Mesh mesh;// = new Mesh();
    MeshCollider mCollider;
    void Start()
    {

        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.vertices = newVertices.ToArray();
        mesh.uv = newUV;
        mesh.triangles = newTriangles;
        mesh.MarkDynamic();
        mesh.UploadMeshData(false);

        (gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider).sharedMesh = mesh;
        mCollider = GetComponent<MeshCollider>();

    }

        // Update is called once per frame
    void Update ()
    {
        //mesh.Clear();
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.vertices = newVertices.ToArray();
        mCollider.sharedMesh = mesh;
    }
}
