﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerInputActions : PlayerActionSet
{
    // movement actions
    public PlayerAction left;
    public PlayerAction right;
    public PlayerAction up;
    public PlayerAction down;
    public PlayerTwoAxisAction move;

    // orbit actions
    public PlayerAction orbitLeft;
    public PlayerAction orbitRight;
    public PlayerAction orbitUp;
    public PlayerAction orbitDown;
    public PlayerTwoAxisAction orbit;

    // utility actions
    public PlayerAction target;


    public PlayerInputActions()
    {
        left = CreatePlayerAction("Move Left");
        right = CreatePlayerAction("Move Right");
        up = CreatePlayerAction("Move Up");
        down = CreatePlayerAction("Move Down");
        move = CreateTwoAxisPlayerAction(left, right, down, up);

        orbitLeft = CreatePlayerAction("Orbit Left");
        orbitRight = CreatePlayerAction("Orbit Right");
        orbitUp = CreatePlayerAction("Orbit Up");
        orbitDown = CreatePlayerAction("Orbit Down");
        orbit = CreateTwoAxisPlayerAction(orbitLeft, orbitRight, orbitDown, orbitUp);

        target = CreatePlayerAction("Target");
    }

    public static PlayerInputActions CreateWithDefaultBindings()
    {
        var playerInputActions = new PlayerInputActions();

        playerInputActions.up.AddDefaultBinding(Key.W);
        playerInputActions.down.AddDefaultBinding(Key.S);
        playerInputActions.left.AddDefaultBinding(Key.A);
        playerInputActions.right.AddDefaultBinding(Key.D);

        playerInputActions.orbitUp.AddDefaultBinding(Key.UpArrow);
        playerInputActions.orbitDown.AddDefaultBinding(Key.DownArrow);
        playerInputActions.orbitLeft.AddDefaultBinding(Key.LeftArrow);
        playerInputActions.orbitRight.AddDefaultBinding(Key.RightArrow);

        playerInputActions.target.AddDefaultBinding(Key.Space);

        playerInputActions.left.AddDefaultBinding(InputControlType.LeftStickLeft);
        playerInputActions.right.AddDefaultBinding(InputControlType.LeftStickRight);
        playerInputActions.up.AddDefaultBinding(InputControlType.LeftStickUp);
        playerInputActions.down.AddDefaultBinding(InputControlType.LeftStickDown);

        playerInputActions.orbitLeft.AddDefaultBinding(InputControlType.RightStickLeft);
        playerInputActions.orbitRight.AddDefaultBinding(InputControlType.RightStickRight);
        playerInputActions.orbitUp.AddDefaultBinding(InputControlType.RightStickUp);
        playerInputActions.orbitDown.AddDefaultBinding(InputControlType.RightStickDown);

        playerInputActions.target.AddDefaultBinding(InputControlType.RightTrigger);

        return playerInputActions;
    }
}
