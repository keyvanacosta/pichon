﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    PlayerInputActions playerInput;

    public HBird bird;


    void Start()
    {
        playerInput = PlayerInputActions.CreateWithDefaultBindings();
    }


    void Update()
    {
        if(bird == null) { return; }

        bird.moveDir = playerInput.move.Value;
        bird.axisDir = playerInput.orbit.Value;
        bird.isTargeting = playerInput.target.IsPressed;
    }
}
