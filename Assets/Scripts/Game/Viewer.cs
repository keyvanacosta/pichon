﻿using System;

using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Camera))]
public class Viewer : MonoBehaviour
{
    public static Viewer instance { get; protected set; }

    public Vector3 position { get { return transform.position; } set { transform.position = value; } }
    public Vector3 forward { get { return transform.forward; } set { transform.forward = value; } }

    public static Transform Target { set { if (null != instance) instance.target = value; } }
    public Transform target;

    [Range(1, 100)] public float focalDistance = 10;
    [Range(0, 10)] public float focalRadius = 3.37F;
    [Range(0, 32)] public float hardMoveDistance = 16;
    [Range(0, 100)] public float chaseSpeed = 32;

    void Awake()
    {
        instance = this;
    }

    void FixedUpdate()
    {
        if (null != target)
        {
            Vector3 D = target.position - position;
            D = D - forward * Vector3.Dot(D, forward);

            if (D.magnitude > hardMoveDistance + 0.01f)
            {
                D = D - D.normalized * hardMoveDistance;
                position += D;
            }
            else if (D.magnitude > focalRadius)
            {
                float t = D.magnitude / hardMoveDistance;
                float s = chaseSpeed * t;
                position = position + D.normalized * s * Time.deltaTime;
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(new Vector3(0, 0, focalDistance), new Vector3(focalRadius * 2, focalRadius * 2));
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(new Vector3(0, 0, focalDistance), new Vector3(hardMoveDistance * 2, hardMoveDistance * 2));

        if (null != target)
        {
            Vector3 D = target.position - transform.position;
            D = D - transform.forward * Vector3.Dot(D, transform.forward);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(Vector3.zero, D);
        }
    }
}
