﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeGameController : MonoBehaviour
{

    //public Transform startPos;

    public GameObject snakePart_PreFab;

    public GameObject[] snake;

    public Color snakeColor = Color.white;

    public int snakeSize = 0;

    public int snakeDirection = 0;

    public float snakeEyeDistance = 1;

    float timer = 0;

    public bool dead = false;

    public bool uninitialized = true;

    public float delayTime = 0.01f;

    public int[] hits = new int[4]{0,0,0,0 };

    private void Awake()
    {
        //startPos = transform;
    }

    IEnumerator createSnake()
    {
        timer = Time.time;
        if (snakeSize > 0)
        {
            snake = new GameObject[snakeSize];

            SnakePartComponent spc;
            for (int i = 0; i < snake.Length; i++)
            {
                //yield return new WaitForSeconds(delayTime);
                snake[i] = Instantiate(snakePart_PreFab);
                //snake[i].GetComponent<Renderer>().material.color = snakeColor;// gameObject.GetComponent<Renderer>().material.color;
                snake[i].transform.GetChild(1).GetComponent<Renderer>().material.color = snakeColor;
                snake[i].name += i.ToString();
                spc = snake[i].GetComponent<SnakePartComponent>();
                spc.Index = i;
                spc.tailPos = Vector3.right;
                spc.pos = Vector3.zero + gameObject.transform.position;
                int tempI = i;
                if (i == 0 )
                {
                    snake[0].transform.position = spc.pos;
                    continue;
                }

                spc = snake[tempI-1].GetComponent<SnakePartComponent>();
                spc.pos = snake[tempI-1].transform.position + spc.tailPos;
                spc.tailPos = Vector3.right;
                snake[tempI].transform.position = spc.pos;
            }

        }
        yield return new WaitForSeconds(delayTime);
        uninitialized = false;
    }






    public void Death()
    {
        Debug.Log("Death");
        dead = true;
    }

    //private void FixedUpdate()
    //{
    //    Vector3 directionVector = Vector3.zero;
    //    switch (snakeDirection)
    //    {
    //        case 0:
    //            directionVector = Vector3.forward;
    //            break;
    //        case 1:
    //            directionVector = Vector3.left;
    //            break;
    //        case 2:
    //            directionVector = Vector3.back;
    //            break;
    //        case 3:
    //            directionVector = Vector3.right;
    //            break;
    //        default:
    //            break;
    //    }
        
    //    if (!uninitialized)
    //    {
    //        Ray myRay = new Ray(snake[0].transform.position, Vector3.forward);
    //        myRay = new Ray(snake[0].transform.position, directionVector);
    //        Debug.DrawRay(myRay.origin, myRay.direction * snakeEyeDistance, Color.black);
    //    }
    //}

    public Vector3 moveHead(int newDirection, int timesHit = -1)
    {       
        SnakePartComponent spc = snake[0].GetComponent<SnakePartComponent>();
        Ray myRay = new Ray(transform.position, Vector3.forward);
        RaycastHit hit;
        Vector3 result = Vector3.zero;
        Vector3 directionVector = Vector3.zero;
        directionVector.y = -1;        
        switch (newDirection)
        {
            case 0: //up
                //Debug.Log("up");
                directionVector = Vector3.forward;
                myRay.origin = snake[0].transform.position;
                myRay.direction = Vector3.forward*snakeEyeDistance;
                Debug.DrawRay(myRay.origin, myRay.direction * snakeEyeDistance, Color.yellow);
                if (UnityEngine.Physics.Raycast(myRay, out hit, snakeEyeDistance))
                {
                    switch (hit.collider.tag)
                    {
                        //case "nameOfTag":
                        //    break;
                        default:
                            directionVector.y = 0;
                            break;
                    }   
                }
                else
                {
                    directionVector.y = -1;
                }
                break;
            case 1: //left
                //Debug.Log("left");                
                directionVector = Vector3.left;
                myRay = new Ray(snake[0].transform.position, Vector3.left*snakeEyeDistance );
                Debug.DrawRay(myRay.origin, myRay.direction * snakeEyeDistance, Color.magenta);
                if (UnityEngine.Physics.Raycast(myRay, out hit, snakeEyeDistance))
                {
                    switch (hit.collider.tag)
                    {
                        //case "nameOfTag":
                        //    break;
                        default:
                            directionVector.y = 1;
                            break;
                    }
                }
                else
                {
                    directionVector.y = -1;
                }
                break;
            case 2: //down
                //Debug.Log("down");
                directionVector = Vector3.back;
                myRay = new Ray(snake[0].transform.position, directionVector * snakeEyeDistance);
                Debug.DrawRay(myRay.origin, myRay.direction * snakeEyeDistance, Color.cyan);
                if (UnityEngine.Physics.Raycast(myRay, out hit, snakeEyeDistance))
                {
                    switch (hit.collider.tag)
                    {
                        //case "nameOfTag":
                        //    break;
                        default:
                            directionVector.y = 2;
                            break;
                    }
                }
                else
                {
                    directionVector.y = -1;
                }
                break;
            case 3: //right
                //Debug.Log("right");
                directionVector = Vector3.right;
                myRay = new Ray(snake[0].transform.position, directionVector * snakeEyeDistance);
                Debug.DrawRay(myRay.origin, myRay.direction * snakeEyeDistance, Color.green);
                if (UnityEngine.Physics.Raycast(myRay, out hit, snakeEyeDistance))
                {
                    switch (hit.collider.tag)
                    {
                        //case "nameOfTag":
                        //    break;
                        default:
                            directionVector.y = 3;
                            break;
                    }
                }
                else
                {
                    directionVector.y = -1;
                }
                break;
            default:
                break;
        }
        int storeY = (int)directionVector.y;
        directionVector.y = 0;
        if (storeY < 0)
        {
            snake[0].transform.position += directionVector;
            snake[0].transform.GetChild(1).GetComponent<Renderer>().material.color = snakeColor;
            for (int i = 1; i < snake.Length; i++)
            {
                spc = snake[i - 1].GetComponent<SnakePartComponent>();
                snake[i].transform.GetChild(1).GetComponent<Renderer>().material.color = snakeColor;
                snake[i].transform.position = spc.pos;
                spc.pos = snake[i - 1].transform.position;
                spc.tailPos = snake[i].transform.position;
            }
            return new Vector3(0, -1, 0);
        }
        directionVector.y = storeY;

        return directionVector;
    }

    // Use this for initialization
    void Start () {
        StartCoroutine("createSnake");
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 dir = Vector3.zero;
        int timesHit = 0;
        int tempDir = snakeDirection;
        if (!uninitialized && !dead && Time.time > timer + delayTime)
        {
            while (Mathf.Abs(dir.magnitude) <= 0 + Mathf.Epsilon)// && timesHit < 4)
            {
                dir = moveHead(tempDir);
                tempDir = (int)dir.y;
                if (tempDir < 0)
                {
                    tempDir = snakeDirection;
                    timesHit = 0;
                    hits = new int[4] { 0, 0, 0, 0 };
                    break;
                }
                hits[tempDir] = 1;

                tempDir = UnityEngine.Random.Range(tempDir, 4);
                //Debug.Log("TH:"+timesHit + " TD:" + tempDir);
                if (hits[tempDir] == 1)
                {
                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i] >= 1)
                        {
                            timesHit++;
                            continue;
                        }
                        else
                        {
                            tempDir = i;                          
                            break;
                        }
                    }
                    if (timesHit >= 4)
                    {
                        Death();
                        return;
                    }
                   
                }
                    
                dir = Vector3.zero;
                break;              
            } 
            
            snakeDirection = tempDir;
            timer = Time.time;
        }

        
    }
}
