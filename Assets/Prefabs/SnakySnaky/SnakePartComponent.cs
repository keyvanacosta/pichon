﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakePartComponent: MonoBehaviour
{

    private int my_index;

    public int Index
    {
        get
        {
            return my_index;
        }
        set
        {
            my_index = value;
        }
    }

    public Vector3 pos;
    public Vector3 size;
    public Vector3 tailPos;

    void Awake()
    {
        my_index = 0;
        size = Vector3.one;
        pos = Vector3.zero;
        tailPos = Vector3.right;
    }



}